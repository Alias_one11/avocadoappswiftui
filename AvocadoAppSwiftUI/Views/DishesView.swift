import SwiftUI

struct DishesView: View {
    
    var body: some View {
        
        /**|CONTAINER-STACK FOR ALL|*/
        HStack(alignment: .center, spacing: 4) {
            
           /**|1ST COLUMN--> HStack inside helper function 😉|*/
            VStack(alignment: .leading, spacing: 4) {
                // HStack() {
                /**|TOAST|*/
                iconsLeftSetupUI(imgName: "icon-toasts", desc: "Toast")
                Divider()
                /**|TACOS|*/
                iconsLeftSetupUI(imgName: "icon-tacos", desc: "Tacos")
                Divider()
                /**|SALAD|*/
                iconsLeftSetupUI(imgName: "icon-salads", desc: "Salad")
                Divider()
                /**|SPREADS|*/
                iconsLeftSetupUI(imgName: "icon-halfavo", desc: "Spreads")
                // }
            }
            
            /**|2ND COLUMN|*/
            /**|HEART|*/
            VStack(alignment: .center, spacing: 16) {
                HStack { Divider() }
                /*©-----------------------------------------©*/
                Image(systemName: "heart.circle")
                    .font(Font.title.weight(.ultraLight))
                    .imageScale(.large)
                /*©-----------------------------------------©*/
                HStack { Divider() }
            }
            
            /**|3RD COLUMN--> HStack inside helper function 😉|*/
            VStack(alignment: .trailing, spacing: 4) {
                // HStack() {
                /**|GUACAMOLE|*/
                iconsRightSetupUI(desc: "icon-guacamole", imgName: "Guacamole")
                Divider()
                /**|SANDWICH|*/
                iconsRightSetupUI(desc: "icon-sandwiches", imgName: "Sandwich")
                Divider()
                /**|SOUP|*/
                iconsRightSetupUI(desc: "icon-soup", imgName: "Soup")
                Divider()
                /**|SMOOTHIE|*/
                iconsRightSetupUI(desc: "icon-smoothies", imgName: "Smoothie")
                // }
            }
        }// END OF HSTACK
        .font(.system(.callout, design: .serif)).foregroundColor(.gray)
        .padding(.horizontal).frame(maxHeight: 220)
    }
    
    // MARK: - Helper methods
    /*©-----------------------------------------©*/
    // MARK: -#typealias for function return types(Waaay to long-->😎)
    typealias LeftReturn = HStack<TupleView<(ModifiedContent<Image, IconModifier>, Spacer, Text)>>
    
    typealias RightReturn = HStack<TupleView<(Text, Spacer, ModifiedContent<Image, IconModifier>)>>
    
    // LEFT ICONS
    fileprivate func iconsLeftSetupUI(imgName: String, desc: String) -> LeftReturn {
        return HStack() {
            Image(imgName).resizable().modifier(IconModifier())
            Spacer()// Spaced horizontally
            Text(desc)
        }
    }
    
    // RIGHT ICONS
    fileprivate func iconsRightSetupUI(desc: String, imgName: String) -> RightReturn {
        return HStack() {
            Text(imgName); Spacer()// Spaced horizontally
            Image(desc).resizable().modifier(IconModifier())
        }
    }
    /*©-----------------------------------------©*/
    
}// END OF STRUCT

struct DishesView_Previews: PreviewProvider {
    static var previews: some View {
        DishesView().previewLayout(.fixed(width: 414, height: 280))
    }
}
