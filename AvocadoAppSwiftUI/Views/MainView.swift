import SwiftUI

struct MainView: View {
    // MARK: - ©Properties
    
    
    var body: some View {
        
        TabView {
            
            AvocadoView()
                .tabItem {
                    Image("tabicon-branch")
                    // Stacked horizontally
                    Text("Avocados")
                }
            /**|__________|*/
            ContentView()
                .tabItem {
                    Image("tabicon-book")
                    // Stacked horizontally
                    Text("Recipes")
                }
            /**|__________|*/
            RipeningStagesView()
                .tabItem {
                    Image("tabicon-avocado")
                    // Stacked horizontally
                    Text("Ripening")
                }
            /**|__________|*/
            SettingsView()
                .tabItem {
                    Image("tabicon-settings")
                    // Stacked horizontally
                    Text("Settings")
                }
            
        }/// END OF TABVIEW
        .accentColor(.primary)
    }
}// END OF STRUCT

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
            
    }
}
