import SwiftUI

struct RecipesDetailView: View {
    // MARK: - ©PROPERTIES
    var recipe: Recipe
    
    // MARK: - ©State, @
    @State private var pulsateAnimation: Bool = false
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack(alignment: .center, spacing: 0) {
                Image(recipe.image).resizable().scaledToFit()
                
                Group {
                    
                    // TITLE
                    TitleModifier.titlesModifierFunc(title: recipe.title, txtStyle: .largeTitle)
                        .multilineTextAlignment(.center).padding(.top, 10)
                    
                    // RATING
                    RecipeRatingView(recipe: recipe)
                    
                    // COOKING
                    RecipeCookingView(recipe: recipe)
                    
                    // INGREDIENTS
                    TitleModifier.titlesModifierFunc(title: "Ingredients")
                    /**|     ⇪     |*/
                    VStack(alignment: .leading, spacing: 5) {
                        /// ||__________||
                        ForEach(recipe.ingredients, id: \.self) { item in
                            /// ||__________||
                            VStack(alignment: .leading, spacing: 5) {
                                TitleModifier.titlesModifierFunc(title: item, txtStyle: .footnote)
                                    .multilineTextAlignment(.leading)
                                Divider()// Divides the footnotes vertically
                            }
                        }
                    }
                    
                    // INSTRUCTIONS
                    TitleModifier.titlesModifierFunc(title: "Instructions")
                    
                    ForEach(recipe.instructions, id: \.self) { item in
                        
                        VStack(alignment: .center, spacing: 5) {
                            configuredChevronImgFunc(sysImgSF: "chevron.down.circle")
                            /**|     ⇪     |*/
                            TitleModifier.titlesModifierFunc(title: item, txtStyle: .body)
                                .lineLimit(nil).multilineTextAlignment(.center)
                                .frame(minHeight: 100)
                        }
                    }
                    
                }/// END OF GROUP
                .padding(.horizontal, 24).padding(.vertical, 12)
            }
        }.edgesIgnoringSafeArea(.top)/// END OF SCROLLVIEW
        /**|     ⇪     |*/
        .overlay(
            
            HStack {
                Spacer()// Moves the button to the right horizontally
                /// ||__________||
                VStack {
                    configuredChvronBtnFunc(sysImgSF: "chevron.down.circle.fill")
                    /// ||__________||
                    Spacer()// Moves the button to the top veritcally
                }
            }
        )/// END OF OVERLAY
    
        /// ||ANIMATION||
        .onAppear {
            pulsateAnimation.toggle()
        }
    }
    
    // MARK: - Helper methods
    fileprivate func configuredChevronImgFunc(sysImgSF: String) -> some View {
        return // CHEVRON IMAGE
            Image(systemName: sysImgSF).resizable()
            .frame(width: 42, height: 42, alignment: .center)
            .imageScale(.large).font(Font.title.weight(.ultraLight))
            .foregroundColor(Color("ColorGreenAdaptive"))
    }
    
    fileprivate func configuredChvronBtnFunc(sysImgSF: String) -> some View {
        return Button(action: {
            // ACTION - Will dismiss the `RecipesDetailView` when the button is tapped
            presentationMode.wrappedValue.dismiss()
        }, label: {
            Image(systemName: sysImgSF).font(.title)
                .foregroundColor(.white).shadow(radius: 4)
        })/// END OF BUTTON
        .padding(.trailing, 20).padding(.top, 24)
        // ANIMATION onAppear
        .opacity(pulsateAnimation ? 1 : 0.6)
        .scaleEffect(pulsateAnimation ? 1.2 : 0.8, anchor: .center)
        .animation(Animation.easeInOut(duration: 1.5).repeatForever(autoreverses: true))
    }
    
}// END OF STRUCT

struct RecipesDetailView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let recipesSubScript = recipesData[0]
    
    static var previews: some View {
        /**|__________|*/
        RecipesDetailView(recipe: recipesSubScript)
            .previewLayout(.fixed(width: 420, height: 780))
    }
}
