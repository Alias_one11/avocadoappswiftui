import SwiftUI

struct FactsView: View {
    // MARK: - ©PROPERTIES
    var fact: Fact
    
    var body: some View {
        
        ZStack {
            /**|CARD-TEXT LOGIC|*/
            cardTxtLogic(with: fact.content)
            
            /**|OFF-SET IMAGE TO THE LEFT|*/
            cardImgOffsetFunc(imgName: fact.image).offset(x: -150)
        }// END OF ZSTACK
    }
    
    // MARK: - Helper methods
    fileprivate func cardImgOffsetFunc(imgName: String) -> some View {
        return Image(imgName).resizable()
            .frame(width: 66, height: 66, alignment: .center)
            .clipShape(Circle())
            /**|ZSTACK Circle()#1|*/
            .background(
                Circle().fill(Color.white)
                    .frame(width: 74, height: 74, alignment: .center)
            )
            /**|ZSTACK Circle()#2|*/
            .background(
                Circle().fill(
                    LinearGradient(gradient: Gradient(colors: [
                        Color("ColorGreenMedium"),
                        Color("ColorGreenLight")
                    ]),
                    startPoint: .trailing, endPoint: .leading))
                    .frame(width: 82, height: 82, alignment: .center)
            )
            /**|ZSTACK Circle()#3|*/
            .background(
                Circle().fill(Color("ColorAppearanceAdaptive"))
                    .frame(width: 90, height: 90, alignment: .center)
            )
    }
    
    fileprivate func cardTxtLogic(with cardTxt: String) -> some View {
        return Text(cardTxt)
            .padding(.leading, 55).padding(.trailing, 10).padding(.vertical, 3)
            .frame(width: 300, height: 135, alignment: .center)
            .background(
                LinearGradient(gradient: Gradient(colors: [
                    Color("ColorGreenMedium"),
                    Color("ColorGreenLight")
                ]),
                startPoint: .leading, endPoint: .trailing))
            .cornerRadius(12).lineLimit(6).multilineTextAlignment(.leading)
            .font(.footnote).foregroundColor(.white)
    }
}// END OF STRUCT

struct FactsView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let factSubscript = factData[0]
    
    static var previews: some View {
        
        FactsView(fact: factSubscript)
            .previewLayout(.fixed(width: 400, height: 220))
    }
}
