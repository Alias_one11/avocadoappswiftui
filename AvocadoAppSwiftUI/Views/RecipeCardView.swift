import SwiftUI

struct RecipeCardView: View {
    // MARK: - ©PROPERTIES
    var recipe: Recipe
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    
    // MARK: - ©State
    @State private var showModal: Bool = false
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 0) {
            /**|CARD-IMAGE|*/
            Image(recipe.image).resizable().scaledToFit()
                // .overlay:--? Large secondary view, in front of this view
                .overlay(
                    HStack {
                        Spacer()// Moves the bookmark horizontally to the right
                        /**|__________|*/
                        VStack {
                            /**|BOOKMARK|*/
                            configuredBookmarkFunc()
                         /**|__________|*/
                         Spacer()// Will push our bookmark sfSymbol up vertically
                        }
                    }
                )
            
            VStack(alignment: .leading, spacing: 12) {
                // TITLE
                TitleModifier.titlesModifierFunc(title: recipe.title, txtStyle: .title,
                                                  fgColor: "ColorGreenMedium")
                
                // HEADLINE
                TitleModifier.titlesModifierFunc(title: recipe.headline,
                                                 txtStyle: .body,
                                                 fgColor: "graySkull")
                    .fixedSize(horizontal: false, vertical: true)
               
                // RATING
                RecipeRatingView(recipe: recipe)
                
                // COOKING
                RecipeCookingView(recipe: recipe)
                
            }.padding().padding(.bottom, 12)
            
        }/// END OF VSTACK
        .background(Color.white).cornerRadius(12)
        .shadow(color: Color("ColorBlackTransparentLight"), radius: 8, x: 0, y: 0)
        
        /// UIImpactFeedbackGenerator
        .onTapGesture {
            hapticImpact.impactOccurred()
            // Used for when the user taps on the
            // card we want to present the detail view
            showModal = true
        }.sheet(isPresented: $showModal) {
            RecipesDetailView(recipe: recipe)
        }
    }
    /*©-----------------------------------------©*/
    // MARK: - Helper methods
    fileprivate func configuredBookmarkFunc() -> some View {
        return Image(systemName: "bookmark")
            .font(Font.title.weight(.light)).foregroundColor(.white)
            .imageScale(.small)
            .shadow(color: Color("ColorBlackTransparentLight"),
                    radius: 2, x: 0, y: 0)
            .padding(.trailing, 20)
            .padding(.top, 22)
    }
}// END OF STRUCT

struct RecipeCardView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let recipesSubScript = recipesData[0]
    
    static var previews: some View {
        
        RecipeCardView(recipe: recipesSubScript)
            .previewLayout(.sizeThatFits)
    }
}
