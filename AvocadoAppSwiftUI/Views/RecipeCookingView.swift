 import SwiftUI

struct RecipeCookingView: View {
    // MARK: - ©PROPERTIES
    var recipe: Recipe
    
    var body: some View {
        
        HStack(alignment: .center, spacing: 20) {
            /// SERVING-SIZE
            // HStack(alignment: .center, spacing: 2)
            customerInfoFunc(imgName: "person.2", txt: "Serves: \(recipe.serves)")
            /// PREPARATION
            // HStack(alignment: .center, spacing: 2)
            customerInfoFunc(imgName: "clock", txt: "Prep: \(recipe.preparation) min")
            /// COOKING TIME
            // HStack(alignment: .center, spacing: 2)
            customerInfoFunc(imgName: "flame", txt: "Cooking: \(recipe.cooking) min")
                .fixedSize(horizontal: false, vertical: true)
            /// ||__________||
        }.fixedSize(horizontal: false, vertical: true)
        .font(.footnote).foregroundColor(.gray)
    }// END OF STRUCT
    
    // MARK: - Helper methods
    fileprivate func customerInfoFunc(imgName: String, txt: String) -> HStack<TupleView<(Image, Text)>> {
        return
            HStack(alignment: .center, spacing: 2) {
                
                Image(systemName: imgName)
                Text(txt)
            }
    }
    
}// END OF STRUCT

struct RecipeCookingView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let recipesSubScript = recipesData[0]
    
    static var previews: some View {
        RecipeCookingView(recipe: recipesSubScript)
            .previewLayout(.fixed(width: 420, height: 100))
    }
}
