import SwiftUI

struct RipeningStagesView: View {
    // MARK: - ©PROPERTIES
    var ripeningStages: [Ripening] = ripeningData
    
    var body: some View {
        
        ScrollView(.horizontal, showsIndicators: false) {
            
            VStack {
                
                Spacer()
                /**|Iterates through the data in function|*/
                ripeningStagesFunc(stages: ripeningStages)
                    .padding(.vertical)
                    .padding(.horizontal, 25)
                Spacer()
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
    
    // MARK: - Helper methods
    /*©-----------------------------------------©*/
    typealias RipeningReturn = HStack<ForEach<[Ripening], String, RipeningView>>
    
    fileprivate func ripeningStagesFunc(stages: [Ripening]) -> RipeningReturn {
        return HStack(alignment: .center, spacing: 25) {
            /**|Iterating through the data|*/
            ForEach(stages) { item in
                RipeningView(ripening: item)
            }
        }
    }
    
}// END OF STRUCT

struct RipeningStagesView_Previews: PreviewProvider {
    static var previews: some View {
        RipeningStagesView(ripeningStages: ripeningData)
            .previewLayout(.fixed(width: 420, height: 620))
            .preferredColorScheme(.dark)
    }
}
