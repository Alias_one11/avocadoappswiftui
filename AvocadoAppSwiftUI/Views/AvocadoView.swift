import SwiftUI

struct AvocadoView: View {
    // MARK: - ©PROPERTIES
    @State private var pulsatingAnimation: Bool = false
    
    var body: some View {
        
        VStack {
            Spacer()
            
            Image("avocado").resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 240, height: 240, alignment: .center)
                .shadow(color: Color("ColorBlackTransparentDark"),
                        radius: 12, x: 0, y: 8)
                // Gives the image animation
                .scaleEffect(pulsatingAnimation ? 1 : 0.9)
                .opacity(pulsatingAnimation ? 1 : 0.9)
                .animation(
                    Animation.easeInOut(duration: 1.5)
                        .repeatForever(autoreverses: true))
            
            VStack {
                Text("Avocados".uppercased())
                    .font(.system(size: 42, weight: .bold, design: .serif))
                    .foregroundColor(.white).padding()
                    .shadow(color: Color("ColorBlackTransparentDark"),
                            radius: 4, x: 0, y: 4)
                
                Text(
                    "Creamy, green, and full of nutrients! Avocado is" +
                    " a powerhouse ingredient at any meal. Enjoy these" +
                    " handpicked avocado recipes for breakfast," +
                    " lunch, dinner & more!"
                )
                .lineLimit(nil).font(.system(.headline, design: .serif))
                .foregroundColor(Color("ColorGreenLight"))
                .multilineTextAlignment(.center).lineSpacing(10)
                .frame(maxWidth: 640, minHeight: 120)
                
            }.padding()// END OF VSTACK
            Spacer()
            
        }.background(Image("background").resizable()
                        .aspectRatio(contentMode: .fill))
        .edgesIgnoringSafeArea(.all)
        // TOGGLES ANIMATION
        .onAppear {
            self.pulsatingAnimation.toggle()
        }
        
    }
}


struct AvocadoView_Previews: PreviewProvider {
    static var previews: some View {
        AvocadoView().preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 420, height: 720))
    }
}
