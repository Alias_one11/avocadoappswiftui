import SwiftUI

struct RipeningView: View {
    // MARK: - ©PROPERTIES
    var ripening: Ripening
    
    // MARK: - ©State
    @State private var slideInAnimation: Bool = false
    
    var body: some View {
        
        VStack {
            // AVOCADO-SFIMAGE
            configuredAvocadoImgFuncWith(sfImageTxt: ripening.image)
                // Handling animation
                .animation(Animation.easeInOut(duration: 1))
                .offset(y: slideInAnimation ? 55 : -55)
            
            VStack(alignment: .center, spacing: 10) {
                // 1-->STAGE
                configuredStageTxtFunc(titleTxt: ripening.stage, titleTxt2: "STAGE")
                    .padding(.top, 65).frame(width: 180)
                    .fixedSize(horizontal: false, vertical: true)
                
                // TITLE-->Hard
                configuredHardTxtFunc(titleTxt: ripening.title)
                    .fixedSize(horizontal: false, vertical: true)
                
                // DESCRIPTION
                Spacer()
                configuredDescriptionTxtFunc(titleTxt: ripening.description, fgColor: "ColorGreenDark").fixedSize(horizontal: false, vertical: true)
                Spacer()
                
                // RIPENESS
                configuredRipenessTxtFunc(titleTxt: ripening.ripeness.uppercased())
                    // Makes sure your text wraps correctly & does not give you the ...
                    // instead of actually wrapping the text .lineLimit() does not work
                    .fixedSize(horizontal: false, vertical: true)
                
                // INSTRUCTIONS
                TitleModifier.titlesModifierFunc(title: ripening.instruction,
                                                 txtStyle: .footnote,
                                                 fontWeight: .bold,
                                                 fgColor: "ColorGreenLight")
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
                
            }.zIndex(0).multilineTextAlignment(.center).padding(.horizontal)
            .frame(width: 260, height: 485, alignment: .center)
            .background(
                LinearGradient(gradient: Gradient(colors: [
                    Color("ColorGreenLight"),
                    Color("ColorGreenMedium")
                ]),
                startPoint: .top, endPoint: .bottom))
            .cornerRadius(20)
            
        }.edgesIgnoringSafeArea(.all)
        // Handling animation
        .onAppear {
            slideInAnimation.toggle()
        }
        
    }
    
    // MARK: - Helper method
    /*©-----------------------------------------©*/
    fileprivate func configuredHardTxtFunc(titleTxt: String) -> some View {
        /**|     ⇪     |*/
        return TitleModifier.titlesModifierFunc(title: titleTxt,
                                                txtStyle: .title,
                                                fontWeight: .bold,
                                                fgColor: "ColorGreenMedium")
            /**|     ⇪     |*/
            .padding(.vertical, 12).padding(.horizontal, 0)
            .frame(width: 220)
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .fill(
                        LinearGradient(gradient: Gradient(colors: [
                            .white ,
                            Color("ColorGreenLight")
                        ]), startPoint: .top, endPoint: .bottom))
                    .shadow(color: Color("ColorBlackTransparentLight"),
                            radius: 6, x: 0, y: 6)
            )
    }
    
    fileprivate func configuredStageTxtFunc(titleTxt: String, titleTxt2: String) -> some View {
        return VStack(alignment: .center, spacing: 0) {
            TitleModifier.titlesModifierFunc(title: titleTxt, txtStyle: .largeTitle)
            /**|     ⇪     |*/
            TitleModifier.titlesModifierFunc(title: titleTxt2,
                                             txtStyle: .body,
                                             fontWeight: .heavy)
            
        }.foregroundColor(Color("ColorGreenMedium"))
    }
    
    fileprivate func configuredDescriptionTxtFunc(titleTxt: String, fgColor: String) -> some View {
        /**|     ⇪     |*/
        return TitleModifier.titlesModifierFunc(title: titleTxt, txtStyle: .body,
                                                fontWeight: .bold,
                                                fgColor: fgColor)
    }
    
    fileprivate func configuredRipenessTxtFunc(titleTxt: String) -> some View {
        /**|     ⇪     |*/
        return TitleModifier.titlesModifierFunc(title: titleTxt, txtStyle: .callout,
                                                fontWeight: .bold, fgColor: "white")
            .lineLimit(4)
            .shadow(radius: 3).padding(.vertical).padding(.horizontal, 0)
            .frame(width: 185)
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .fill(
                        LinearGradient(gradient: Gradient(colors: [
                            Color("ColorGreenMedium"),
                            Color("ColorGreenDark")
                        ]), startPoint: .top, endPoint: .bottom))
                    .shadow(color: Color("ColorBlackTransparentLight"),
                            radius: 6, x: 0, y: 6)
            )
    }
    
    fileprivate func configuredAvocadoImgFuncWith(sfImageTxt: String) -> some View {
        return Image(sfImageTxt).resizable()
            .frame(width: 100, height: 100, alignment: .center).clipShape(Circle())
            .background(
                Circle().fill(Color("ColorGreenLight"))
                    .frame(width: 110, height: 110, alignment: .center)
                /// ||__________||
            )
            .background(
                Circle().fill(Color("ColorAppearanceAdaptive"))
                    .frame(width: 120, height: 120, alignment: .center)
                /**|__________|*/
            ).zIndex(1).offset(y: 55)
    }
    /*©-----------------------------------------©*/
    
}// END OF STRUCT

struct RipeningView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let ripeningSubScript = ripeningData[1]
    
    static var previews: some View {
        
        RipeningView(ripening: ripeningSubScript)
            .previewLayout(.fixed(width: 420, height: 720))
    }
}
