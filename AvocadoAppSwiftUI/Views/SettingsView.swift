import SwiftUI

struct SettingsView: View {
    // MARK: - ©State-->PROPERTIES
    @State private var enableNotification: Bool = true
    @State private var bgRefresh: Bool = false
    
    var body: some View {
        
        VStack(alignment: .center, spacing: 0) {
            /**|     ⇪     |*/
            VStack(alignment: .center, spacing: 5) {
                // [HEADER] AVOCADO-IMAGE
                configuredImgFunc(image: "avocado")
                
                // TITLE
                TitleModifier.titlesModifierFunc(title: "Avocados".uppercased(), txtStyle: .title,
                                                 fontWeight: .bold, fgColor: "ColorGreenAdaptive")
            }.padding()
            
            Form {
                // MARK: - SECTION #1
                Section(header: Text("General Settings")) {
                    // $ENABLENOTIFICATION
                    Toggle(isOn: $enableNotification) {
                        Text("Enable notifications")
                    }
                    
                    // $BGREFRESH
                    Toggle(isOn: $bgRefresh) {
                        Text("Background refresh")
                    }
                }
                
                // MARK: - SECTION #1
                Section(header: Text("Application")) {
                    
                    if enableNotification {
                        // Product
                        configureAppSectionTxtFunc(txtStr: "Product", txtStr2: "Avocado recipes")
                        // Compatibility
                        configureAppSectionTxtFunc(txtStr: "Compatibility", txtStr2: "Iphone & Ipad")
                        // Developer
                        configureAppSectionTxtFunc(txtStr: "Developer", txtStr2: "Alias / Sami")
                        // Designer
                        configureAppSectionTxtFunc(txtStr: "Designer", txtStr2: "Alisia Huda")
                        // Website
                        configureAppSectionTxtFunc(txtStr: "Website", txtStr2: "alias111@alias.com")
                        // Version
                        configureAppSectionTxtFunc(txtStr: "Version", txtStr2: "1.0.0")
                    } else {
                        HStack {
                            Text("Personal message").foregroundColor(.gray)
                            Spacer()
                            Text("Feliz coding huele bitcho!😝")
                        }
                    }
                }
            }
        }.frame(maxWidth: 640)
    }// END OF STRUCT
    
    // MARK: - Helper methods
    fileprivate func configuredImgFunc(image: String) -> some View {
        return Image(image).resizable().scaledToFit().padding(.top)
            .frame(width: 100, height: 100, alignment: .center)
            .shadow(color: Color("ColorBlackTransparentLight"), radius: 8, x: 0, y: 4)
    }
    
    typealias AppSectionReturn = HStack<TupleView<(Text, Spacer, Text)>>
    
    fileprivate func configureAppSectionTxtFunc(txtStr: String, txtStr2: String) -> AppSectionReturn {
        return HStack {
            Text(txtStr).foregroundColor(.gray)
            Spacer()
            Text(txtStr2)
        }
    }
    
}// END OF STRUCT

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
            .previewLayout(.fixed(width: 420, height: 820))
            .preferredColorScheme(.dark)
    }
}
