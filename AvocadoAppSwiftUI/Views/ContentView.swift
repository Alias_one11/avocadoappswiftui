import SwiftUI

struct ContentView: View {
    // MARK: - ©PROPERTIES
    /*©-----------------------------------------©*/
    var headerList: [Header] = headerData
    var factList: [Fact] = factData
    var recipesList: [Recipe] = recipesData
    /*©-----------------------------------------©*/
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack(alignment: .center, spacing: 20) {
                /**|HEADER-VIEW|*/
                ScrollView(.horizontal, showsIndicators: false) {
                    headerViewIterator()
                }
                
                /**|DISHES-VIEW|*/
                // Title for dishes view
                TitleModifier.titlesModifierFunc(title: "Avocado Dishes")
                DishesView()
                
                /**|FACTS-VIEW|*/
                TitleModifier.titlesModifierFunc(title: "Avocado Facts")
                /**|     ⇪     |*/
                ScrollView(.horizontal, showsIndicators: false) {
                    /**|     ⇪     |*/
                    configuredFactView()
                }
                
                /**|RECIPESCARD-VIEW|*/
                TitleModifier.titlesModifierFunc(title: "Avocado Recipes")
                configuredRecipesCardViewFunc(recipesList: recipesList)
                
                /**|FOOTER-VIEW|*/
                // VStack()
                configuredFooterView(txtTitle: "All About Avocados",
                                     txtBody: "Everything you ever wanted to know" +
                                     " about avocados, but were afraid to ask!")
                
            }// END OF VSTACK
            /**|     ⇪     |*/
        }// END OF SCROLLVIEW
        .edgesIgnoringSafeArea(.all).padding(0)
    }
    
    // MARK: - Helper methods
    /*©-----------------------------------------©*/
    // MARK: -#typealias-->for return type
    typealias HeaderViewIteratorReturn = HStack<ForEach<[Header], String, HeaderView>>
    
    fileprivate func headerViewIterator() -> HeaderViewIteratorReturn {
        return HStack(alignment: .top, spacing: 0) {
            /**|__________|*/
            ForEach(headerList) { item in
                HeaderView(header: item)
            }
        }
    }
    
    fileprivate func configuredFooterView(txtTitle: String, txtBody: String) -> some View {
        return VStack(alignment: .center, spacing: 20) {
            Text(txtTitle).fontWeight(.bold)
                .modifier(TitleModifier())
            
            Text(txtBody)
                .font(.system(.body, design: .serif))
                .multilineTextAlignment(.center)
                .foregroundColor(.gray)
                .frame(minHeight: 60)
            
        }.frame(maxWidth: 640).padding().padding(.bottom, 85)
    }
    
    fileprivate func configuredFactView() -> some View {
        return HStack(alignment: .top, spacing: 60) {
            
            ForEach(factList) { item in
                FactsView(fact: item)
            }
        }.padding(.vertical).padding(.leading, 60).padding(.trailing, 20)
    }
    
    fileprivate func configuredRecipesCardViewFunc(recipesList: [Recipe]) -> some View {
        return /**|     ⇪     |*/
            VStack(alignment: .center, spacing: 20) {
                ForEach(recipesList) { item in
                    RecipeCardView(recipe: item)
                }
            }.frame(maxWidth: 640).padding(.horizontal)
    }
    /*©-----------------------------------------©*/
}// END OF STRUCT

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(headerList: headerData, factList: factData, recipesList: recipesData)
            .preferredColorScheme(.dark)
    }
}
