import SwiftUI

struct RecipeRatingView: View {
    // MARK: - ©PROPERTIES
    var recipe: Recipe
    
    var body: some View {
    
        HStack(alignment: .center, spacing: 5) {
            // Iterating through the stars depending on the rating
            ForEach(1...(recipe.rating), id: \.self) { _ in
                Image(systemName: "star.fill")
                    .font(.body).foregroundColor(.yellow)
            }
        }
    }
}

struct RecipeRatingView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let recipesSubScript = recipesData[0]
    
    static var previews: some View {
        
        RecipeRatingView(recipe: recipesSubScript)
            .previewLayout(.fixed(width: 320, height: 60))
    }
}
