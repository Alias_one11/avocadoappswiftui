import SwiftUI

struct HeaderView: View {
    // MARK: - ©PROPERTIES
    var header: Header
    
    // MARK: - ©State-->PROPERTIES
    @State private var showHeadline: Bool = false
    
    // MARK: - COMPUTED-PROPERTY
    /*©-----------------------------------------©*/
    var slideInAnimation: Animation {
        Animation.spring(response: 1.5, dampingFraction: 0.5, blendDuration: 0.5)
            .speed(1.0).delay(0.25)
    }
    /*©-----------------------------------------©*/
    
    var body: some View {
        
        ZStack {
            /**|HEADER-IMAGE|*/
            Image(header.image).resizable()
                .aspectRatio(contentMode: .fill)
            /**|     ⇪     |*/
            HStack(alignment: .top, spacing: 0) {
                /**|__________|*/
                Rectangle().fill(Color("ColorGreenLight")).frame(width: 4)
                
                VStack(alignment: .leading, spacing: 6) {
                    /**|TITLE|*/
                    Text(header.headline).font(.system(.title, design: .serif))
                        .fontWeight(.bold).foregroundColor(.white).shadow(radius: 3)
                    
                    /**|FOOTNOTE|*/
                    Text(header.subHeadline)
                        .font(.footnote).fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.leading).foregroundColor(.white)
                        .shadow(radius: 3)
                    
                }.padding(.vertical, 0).padding(.horizontal, 20)
                .frame(width: 281, height: 105)
                .background(Color("ColorBlackTransparentLight"))
                
            }.frame(width: 285, height: 105, alignment: .center)
            .offset(x: -66, y: showHeadline ? 75 : 220)
            // Adding our animation. Will animate the view vertically
            .animation(slideInAnimation)
            .onAppear {
                showHeadline.toggle()
            }
            
            
        }.frame(width: 480, height: 320, alignment: .center)
    }
}

struct HeaderView_Previews: PreviewProvider {
    // MARK: - ©Static-->PROPERTY
    static let headerSubScript = headerData[1]
    
    static var previews: some View {
        
        HeaderView(header: headerSubScript)
            .preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
    }
}
