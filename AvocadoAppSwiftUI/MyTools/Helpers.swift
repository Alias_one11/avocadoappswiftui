import SwiftUI

struct IconModifier: ViewModifier {
    // MARK: - Struct methods
    func body(content: Content) -> some View {
        content.frame(width: 42, height: 42, alignment: .center)
    }
}

struct TitleModifier: ViewModifier {
    // MARK: - Struct methods
    func body(content: Content) -> some View {
        content.font(.system(.title, design: .serif))
            .foregroundColor(Color("ColorGreenAdaptive")).padding(8)
    }
    
    static func titlesModifierFunc(title: String, txtStyle: Font.TextStyle = .footnote,
                                   fontWeight:  Font.Weight = .bold,
                                   fgColor: String = "ColorGreenAdaptive") -> some View {
        /**|__________|*/
        return Text(title).font(.system(txtStyle, design: .serif))
            .fontWeight(fontWeight).foregroundColor(Color(fgColor))
    }
}
