import SwiftUI

struct Header: Identifiable {
    // MARK: - ©PROPERTIES
    /*©-----------------------------------------©*/
    var id = UUID().uuidString
    var image: String
    var headline: String
    var subHeadline: String
    /*©-----------------------------------------©*/
    
    
}
