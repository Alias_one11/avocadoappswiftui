import SwiftUI

struct Recipe: Identifiable {
    // MARK: - ©PROPERTIES
    var id = UUID().uuidString
    var title, headline, image: String
    var rating, serves, preparation, cooking: Int
    var instructions, ingredients: [String]
}
