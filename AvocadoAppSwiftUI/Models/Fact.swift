import SwiftUI

struct Fact: Identifiable {
    // MARK: - ©PROPERTIES
    /*©-----------------------------------------©*/
    var id = UUID().uuidString
    var image: String
    var content: String
    /*©-----------------------------------------©*/
    
    
}
