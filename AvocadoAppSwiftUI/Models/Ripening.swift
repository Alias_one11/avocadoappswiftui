import SwiftUI

struct Ripening: Identifiable {
    // MARK: - ©PROPERTIES
    var id = UUID().uuidString
    var image, stage, title, description,
        ripeness, instruction: String
}
